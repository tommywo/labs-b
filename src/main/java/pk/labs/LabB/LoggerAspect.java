/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.labs.LabB;

import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Logger;

/**
 *
 * @author st
 */
@Component("loggeraspect")
public class LoggerAspect{
    
	private Logger log;
        
	@Autowired
	void setLogger(Logger log)
	{
		this.log =log;
	}
        public void logMethodExit(JoinPoint joinPoint, Object result) {
            if (log!=null)
			log.logMethodExit(joinPoint.getSignature().getName(), result);
	}
	
	public void logMethodEntrance(JoinPoint joinPoint) {
            if (log!=null)
			log.logMethodEntrance(joinPoint.getSignature().getName(), joinPoint.getArgs());
	}
}