package pk.labs.LabB.ui;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Display;

@Component("NegativeableDisplay")
public class NegativeableDisplay implements Negativeable {

    Utils utils;

    public NegativeableDisplay() {
        this.utils = new Utils();
    }

    @Override
    public void negative() {
        this.utils.negateComponent(((Display) AopContext.currentProxy()).getPanel());
    }
}
