package pk.labs.LabB.ui;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.ControlPanel;

@Component("NegativeableControlPanel")
public class NegativeableControlPanel implements Negativeable {
    Utils utils;
    
    public NegativeableControlPanel() {
        this.utils = new Utils();
    }

    @Override
    public void negative() {
        this.utils.negateComponent(((ControlPanel)AopContext.currentProxy()).getPanel());
    }
}
