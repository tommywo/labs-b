package pk.labs.LabB;

public class LabDescriptor {
    
    // region P1
    public static String displayImplClassName = "pk.labs.LabB.Display.DisplayImpl";
    public static String controlPanelImplClassName = "pk.labs.LabB.controlpanel.ControlPanelImpl";
    
    public static String mainComponentSpecClassName = "pk.labs.LabB.Contracts.Roomba";
    public static String mainComponentImplClassName = "pk.labs.LabB.roomba.RoombaImpl"; 
    public static String mainComponentBeanName = "Roomba";
    // endregion

    // region P2
    public static String mainComponentMethodName = "Start";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    
    
    public static String loggerAspectBeanName = "loggeraspect";
    // endregion
}
